<?php
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use \CModule;
use ErrorException;
use \Bitrix\Main\Data\Cache;

if( function_exists('d') === false ) {
	/**
	 * @param $mixed
	 * @param null $collapse
	 * @param bool $bSilent
	 */
	function d($mixed, $collapse = null, $bSilent = false) {
		if($bSilent) {
			echo "<!-- ";

			if ($collapse !== null && is_string($collapse) && strlen($collapse)>0) {
				echo strip_tags($collapse);
			}
			print_r($mixed);
			echo "//-->";
			return;
		}

		static $arCountFuncCall = 0;
		static $arCountFuncCallWithTitleKey = array();
		$arCountFuncCall++;

		$bCollapse = false;
		if($collapse !== null) {
			$bCollapse = true;
			if( is_string($collapse) && strlen($collapse)>0) {
				if( !@isset($arCountFuncCallWithTitleKey[$collapse]) ) {
					$arCountFuncCallWithTitleKey[$collapse] = 0;
				}
				$arCountFuncCallWithTitleKey[$collapse]++;

				$elemTitle = $collapse."#".$arCountFuncCallWithTitleKey[$collapse];
				$elemId = rand(1, 31337).$collapse."#".$arCountFuncCallWithTitleKey[$collapse];
			}
			else {
				$elemTitle = "arResult#".$arCountFuncCall;
				$elemId = rand(1, 31337).$arCountFuncCall;
			}
			$elemId = str_replace(array("'", '"'), "_", $elemId);
		}
		if($bCollapse):?>
			<a	href="javascript:void(0)"
				  style="display: block;background: white; border:1px dotted #5A82CE;padding:3px; text-shadow: none; color: #5A82CE;"
				  onclick="document.getElementById('<?php echo $elemId?>').style.display = ( document.getElementById('<?php echo $elemId?>').style.display == 'none')?'block':'none'"
			>
				<?php echo $elemTitle?>
			</a>
			<div id="<?php echo $elemId?>" style="text-align: left; display:none; background-color: #b1cdef; position: absolute; z-index: 10000;">
		<?php endif?><pre style='font-size: 14px; line-height: 14px;'><?php print_r($mixed);?></pre><?php if ($bCollapse):?></div><?php endif;
	}
}
if( function_exists('GetEntityDataClass') === false ) {
	/**
	 * Получить экземпляр класса HL
	 * @param $HlBlockId - id HighloadBlock
	 * @return array|bool
	 */
	function GetEntityDataClass($HlBlockId) {
		if (empty($HlBlockId) || $HlBlockId < 1) {
			return false;
		}
		$hlblock = HLBT::getById($HlBlockId)->fetch();
		$entity = HLBT::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();
		return $entity_data_class;
	}
}

if( function_exists('getArrAuthor') === false ) {
	/**
	 * Получить массив автора блога
	 * @param $HlBlockId - id HighloadBlock
	 * @return array
	 * @throws ErrorException
	 */
	function getArrAuthor($authorXmlId) {
		$funcResult = array();
		if (!empty($authorXmlId)) {
			if ( CModule::IncludeModule('highloadblock') ) {

				$сache = Cache::createInstance();
				$cacheId = 'getArrAuthor_'.$authorXmlId;
				if ($сache->initCache('86000', $cacheId)) {
					$vars = $сache->GetVars();
					$funcResult = $vars['funcResult'];
				}elseif ($сache->startDataCache()) {
					$entity_data_class = GetEntityDataClass(HLBLOCK_ID__AUTHOR);
					$rsData = $entity_data_class::getList(array('select' => array('*'),'filter' => array('UF_XML_ID' => $authorXmlId)));
					if($el = $rsData->fetch()){
						$funcResult = $el;
					}
					$сache->EndDataCache(array(
						'funcResult' => $funcResult,
					));
				}
			} else{
				throw new ErrorException("Не подключен модуль highloadblock");
			}


		}
		return $funcResult;
	}
}




