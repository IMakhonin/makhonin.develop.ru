<?php

// Подключаем глобальные функции
include $_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/functions.php';

// Подключаем глобальные константы
include $_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/constants.php';

// Подключаем события
include $_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/events.php';