<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
/* Пережимаем фотки */
$imgW = 500;
$imgH = 500;

if( !empty($arResult['DETAIL_PICTURE']) ) {
	$arFileTmp = CFile::ResizeImageGet(
		$arResult['DETAIL_PICTURE'],
		array(
			'width' => $imgW,
			'height' => $imgH
		),
		BX_RESIZE_IMAGE_EXACT,
		true
	);
	$arResult['DETAIL_PICTURE'] = array(
		'SRC' =>  $arFileTmp['src'],
		'WIDTH' => $imgW,
		'HEIGHT' => $imgH,
	);
}else{
	$arResult['DETAIL_PICTURE'] = array(
		'SRC' => $this->__folder.'/image/no-image.png',
		'WIDTH' => $imgW,
		'HEIGHT' => $imgH,
	);
}