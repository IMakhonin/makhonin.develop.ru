<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="blog_detail">
	<p class="news-detail__title"><span></span></p>
	<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['NAME']?>">
	<p>Автор статьи из HL: <a href="<?=$arResult['LIST_PAGE_URL']?>?author=<?=$arResult['PROPERTIES']['AUTHOR']['VALUE']?>"><?=$arResult['DISPLAY_PROPERTIES']['AUTHOR']['DISPLAY_VALUE']?></a></p>

	<p>Категория статьи из HL: <span><?=$arResult['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE']?></span></p>
	<p><?=$arResult['DETAIL_TEXT']?></p>
	<? if ( !empty($arResult['TAGS']) ) :?>
		<p>Теги: <?=$arResult['TAGS']?></p>
	<? endif ;?>
</div>