<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
/* Пережимаем фотки */
$imgW = 200;
$imgH = 200;

$arrTmpXmlIdAuthor = array();
foreach($arResult['ITEMS'] as $k => $arItem) {

	if ( !empty( $arItem['PROPERTIES']['AUTHOR']['VALUE'] ) ) {
		$arrTmpXmlIdAuthor[$arItem['PROPERTIES']['AUTHOR']['VALUE']] = $arItem['PROPERTIES']['AUTHOR']['VALUE'];
	}

	if( !empty($arItem['DETAIL_PICTURE']) ) {
		$arFileTmp = CFile::ResizeImageGet(
			$arItem['DETAIL_PICTURE'],
			array(
				'width' => $imgW,
				'height' => $imgH
			),
			BX_RESIZE_IMAGE_EXACT,
			true
		);
		$arResult['ITEMS'][$k]['DETAIL_PICTURE'] = array(
			'SRC' =>  $arFileTmp['src'],
			'WIDTH' => $imgW,
			'HEIGHT' => $imgH,
		);
	}else{
		$arResult['ITEMS'][$k]['PREVIEW_PICTURE'] = array(
			'SRC' => $this->__folder.'/image/no-image.png',
			'WIDTH' => $imgW,
			'HEIGHT' => $imgH,
		);
	}

}


if ( \CModule::IncludeModule('highloadblock') ) {
	if ( !empty( $arrTmpXmlIdAuthor ) ) {
		$entity_data_class = GetEntityDataClass(HLBLOCK_ID__AUTHOR);
		$rsData = $entity_data_class::getList(array('select' => array('*'),'filter' => array('UF_XML_ID' => $arrTmpXmlIdAuthor)));
		while($el = $rsData->fetch()){
			$arResult['AUTHORS'][$el['UF_XML_ID']] = $el;

		}
	}
}