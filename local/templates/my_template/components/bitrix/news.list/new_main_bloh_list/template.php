<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (empty($arResult["ITEMS"])) return; ?>
<div class="main_list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="main_list__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<a class="main_list__title" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
			<p class="main_list__title"><span></span></p>
			<img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
			<p>Автор статьи из HL: <a href="<?=$arItem['LIST_PAGE_URL']?>?author=<?=$arItem['PROPERTIES']['AUTHOR']['VALUE']?>"><?=$arItem['DISPLAY_PROPERTIES']['AUTHOR']['DISPLAY_VALUE']?></a></p>
			<p>Категория статьи из HL: <span><?=$arItem['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE']?></span></p>
			<p><?=$arItem['DETAIL_TEXT']?></p>
		</div>
	<?endforeach;?>
	<?if( ( $arParams["DISPLAY_BOTTOM_PAGER"] ) && ( $arParams['MAIN_PAGE'] != 'Y' ) ):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
