<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;
define('SITE_OLD_PATH', 'http://prostositelight.bitrix.red')
?>

<!DOCTYPE html>
	<html>
		<head>
			<?$APPLICATION->ShowHead();?>
			<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH."/css/main_style.css"?>" type="text/css" />
			<title><?$APPLICATION->ShowTitle();?></title>

		</head>

		<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
			<div id="header">
				<div id="headerin">
					<div id="toplogo">
						<a href="/"><img src="<?=SITE_OLD_PATH?>./bitrix/templates/as_prostosite_gray/img/9e644227a8c49c84950a0a61cea5b8b8.png" /></a>
					</div>
					<div id="topmenu">
						<div id="slogan">Ваш слоган<br />здесь будет располагаться</div>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "new_menu", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
			0 => "",
		),
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	),
	false
);?>

					</div>
					<div id="topphone">+7 (000) 000-00-00</div>
				</div>
			</div>

		<div id="body">
			<div id="workarea">
				<h1><?$APPLICATION->ShowTitle(false)?></h1>
				
